const express = require('express');
const uuidv5 = require('uuid/v5');
const fs = require('fs');

const router = express.Router();

router.post('/synthesize', function(req, res) {
  const textToSpeech = req.textToSpeech;
  
  const synthesizeParams = {
    text: req.body.text,
    accept: 'audio/wav',
    voice: 'es-LA_SofiaVoice',
  };

  textToSpeech
  .synthesize(synthesizeParams)
  .then(response => {
    const audio = response.result;
    return textToSpeech.repairWavHeaderStream(audio);
  })
  .then(repairedFile => {
    const name = `audios/${uuidv5.URL}.wav`;
    fs.writeFileSync(name, repairedFile);
    return res.json({
      audio: name
    });
  })
  .catch(err => {
    return res.json({
      message: err
    });
  });
});

module.exports = router;