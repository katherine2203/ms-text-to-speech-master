const express = require('express');
const cfenv = require("cfenv");
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require("cors");

const app = express();
const appEnv = cfenv.getAppEnv();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

dotenv.config();

app.use(require('./middleware'));
app.use('/', require('./routes'));

app.listen(appEnv.port, appEnv.bind, function() {
  console.log("server starting on " + appEnv.url);
});